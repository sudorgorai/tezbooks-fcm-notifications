import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future<NotificationDetails?> platformChannelSpecifics() async {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('launch_background');

  const DarwinInitializationSettings initializationSettingsIOS =
      DarwinInitializationSettings(
    requestSoundPermission: false,
    requestBadgePermission: false,
    requestAlertPermission: false,
  );

  const InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: null);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  const AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails(
          "String", //Required for Android 8.0 or after
          "String", //Required for Android 8.0 or after
          channelDescription: "String", //Required for Android 8.0 or after
          importance: Importance.high,
          priority: Priority.max);

  const DarwinNotificationDetails iOSPlatformChannelSpecifics =
      DarwinNotificationDetails(
          presentAlert:
              true, // Present an alert when the notification is displayed and the application is in the foreground (only from Darwin 10 onwards)
          presentBadge:
              true, // Present the badge number when the notification is displayed and the application is in the foreground (only from Darwin 10 onwards)
          presentSound:
              true, // Play a sound when the notification is displayed and the application is in the foreground (only from Darwin 10 onwards)
          sound:
              "String", // Specifics the file path to play (only from Darwin 10 onwards)
          badgeNumber: 0 // The application's icon badge number
          );

  const NotificationDetails platformChannelSpecifics = NotificationDetails(
      android: androidPlatformChannelSpecifics,
      iOS: iOSPlatformChannelSpecifics);

  return platformChannelSpecifics;
}
