import 'dart:math';

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'firebase_options.dart';
import 'platform_channel.dart';
// import 'local_notifications_init.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.reload();

  var notificationCounter = prefs.getString('notificationCounter') ?? '{}';
  var notificationMap = jsonDecode(notificationCounter);

  if (notificationMap[message.data['company_ID']] == null) {
    notificationMap[message.data['company_ID']] = {};
    notificationMap[message.data['company_ID']]['counter'] = '0';
    notificationMap[message.data['company_ID']]['name'] = '';
  }

  var companyCounter =
      int.parse(notificationMap[message.data['company_ID']]['counter']);
  companyCounter++; // Increase the notification counter when a new notification is received
  notificationMap[message.data['company_ID']]['counter'] =
      companyCounter.toString();
  notificationMap[message.data['company_ID']]['name'] =
      message.data['company_name'];

  prefs.setString(
      'notificationCounter',
      jsonEncode(
          notificationMap)); // Update the increment in shared preferences

  notificationMap.forEach((key, value) async {
    var companyName = value['name'];
    var counter = value['counter'];

    if (int.parse(counter) > 1 || message.data['company_ID'] != key) {
      // If more than 1 notification received for this company or a single notification was received previously, then display a single grouped notification
      await flutterLocalNotificationsPlugin
          .cancelAll(); // Dismiss all previous notifications

      await flutterLocalNotificationsPlugin.show(
          Random().nextInt(1 << 31),
          message.data['title'],
          "You have $counter pending transactions for company $companyName",
          await platformChannelSpecifics());
    } else {
      // if a single notification is received for this company, display it
      await flutterLocalNotificationsPlugin.show(
          message.messageId.hashCode,
          message.data['title'],
          message.data['body'],
          await platformChannelSpecifics());
    }
  });
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  final messaging = FirebaseMessaging.instance;

  String? token;
  token = await messaging.getToken();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  void sendFCMToken(token, currentTime) async {
    final http.Response response = await http.post(
      Uri.parse(
          'https://d1who55cbx0x6d.cloudfront.net/v1/auth0|631f17b1f0a0ab71c69ccb29/add_fcm_token'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "token": token,
        "expiry_timestamp": currentTime,
      }),
    );
  }

  int? fcmTokenTimestamp = prefs.getInt('FCMTokenTimestamp');
  var currentTime = DateTime.now().millisecondsSinceEpoch;
  if (fcmTokenTimestamp == null) {
    // Timestamp does not exist in Shared Preference
    // Post installation

    // Update timestamp
    prefs.setInt('FCMTokenTimestamp', currentTime);
    // Update database
    sendFCMToken(token, currentTime + 30 * 24 * 60 * 60 * 1000);
  } else {
    var fcmTokenDateObject =
        DateTime.fromMillisecondsSinceEpoch(fcmTokenTimestamp);
    Duration age = DateTime.now().difference(fcmTokenDateObject);
    if (age.inDays > 30) {
      // If token has expired

      // Update timestamp
      prefs.setInt('FCMTokenTimestamp', currentTime);

      // Update token
      await FirebaseMessaging.instance.deleteToken();
      var newToken = await FirebaseMessaging.instance.getToken();

      // Update database
      sendFCMToken(newToken, currentTime + 30 * 24 * 60 * 60 * 1000);
    }
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  String _lastMessage = "";

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    print("initstate()");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        print("Resumed");
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('notificationCounter', "{}");
        break;
      case AppLifecycleState.inactive:
        print("Inactive");
        break;
      case AppLifecycleState.paused:
        print("Paused");
        break;
      case AppLifecycleState.detached:
        print("Detached");
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('Last message from Firebase Messaging:',
                style: Theme.of(context).textTheme.titleLarge),
            Text(_lastMessage, style: Theme.of(context).textTheme.bodyLarge),
          ],
        ),
      ),
    );
  }
}
