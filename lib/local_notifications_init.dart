import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';

void backgroundNotificationTap(
    NotificationResponse notificationResponse) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setInt('notificationCounter', 0);
  prefs.reload();
}

void initializeFlutterLocalNotifications() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('launch_background');
  const DarwinInitializationSettings initializationSettingsDarwin =
      DarwinInitializationSettings();
  const InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsDarwin);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onDidReceiveNotificationResponse:
          (NotificationResponse notificationResponse) async {
    prefs.setInt('notificationCounter',
        0); // Reset notification counter to 0 when user clicks on the notification
  }, onDidReceiveBackgroundNotificationResponse: backgroundNotificationTap);
}
